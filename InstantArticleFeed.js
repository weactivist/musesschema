'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let InstantArticleFeedSchema = new Schema({
    title: String,
    link: String
});

module.exports = mongoose.model('InstantArticleFeed', InstantArticleFeedSchema);

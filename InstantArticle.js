'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let InstantArticleSchema = new Schema({
    _id: {
        type: String,
        validate: {
            validator: function(v) {
                if(!v) {
                    return true;
                }

                return /^[a-zA-Z0-9]{40}$/.test(v);
            },
            message: '{VALUE} is not a valid ID!'
        }
    },
    title: {
        type: String,
        required: 'Title is missing.'
    },
    link: {
        type: String,
        required: 'Link is missing.'
    },
    description: String,
    author: String,
    guid: String,
    pubDate: Date,
    content: {
        type: String,
        required: 'Content is missing.'
    },
    status: {
        type: String,
        enum: ['error', 'waiting', 'valid', 'success'],
        default: 'waiting'
    },
    error: String
});

module.exports = mongoose.model('InstantArticle', InstantArticleSchema);

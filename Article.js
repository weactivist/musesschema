'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ArticleSchema = new Schema({
    title: {
        type: String,
        required: 'Title is missing.'
    },
    link: {
        type: String,
        required: 'Link is missing.'
    },
    description: String,
    author: String,
    guid: String,
    pubDate: Date,
    content: {
        type: String,
        required: 'Content is missing.'
    },
    type: {
        type: String,
        enum: ['ia', 'amp']
    },
    refId: String
});

module.exports = mongoose.model('Article', ArticleSchema);
